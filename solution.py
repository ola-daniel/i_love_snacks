import json

class Snacker:
  def __init__(self, email, fave_snack, price):
    self.__email__ = email
    self.__fave_snack__ = fave_snack
    self.__fave_snack_price__ = float(price)

  def email(self):
    return self.__email__

  def fave_snack(self):
    return self.__fave_snack__

  def fave_snack_price(self):
    return self.__fave_snack_price__


class Product:
  def __init__(self, title, price):
    self.__title__ = title
    self.__price__ = float(price)

  def title(self):
    return self.__title__

  def price(self):
    return self.__price__


#load product data
with open('./products.json', 'r') as products:
  productData = json.loads(products.read())

  prods = []
  for p in productData['products']:
    prods.append(Product(p['title'], p['variants'][0]['price']))

#load snacker data
with open('./MOCK_SNACKER_DATA.json', 'r') as mock_snacker_data:
  snacker_data = json.loads(mock_snacker_data.read())

snackers = []
for i in snacker_data:
  for product in prods:
    if product.title().lower() == i['fave_snack'].lower():
      snackers.append(Snacker(i['email'], i['fave_snack'], product.price()))

#a) List the real stocked snacks you found under the snacker's 'fave_snack'?
#b) What're the emails of the snackers who listed those as a 'fave_snack'?
#c) If all those snackers we're to pay for their 'fave_snack'what's the total price?

real_stocked_snacks = []
snackers_email = []
total_price = 0

for snacker in snackers:
  real_stocked_snacks.append(snacker.fave_snack())
  snackers_email.append(snacker.email())
  total_price += snacker.fave_snack_price()

real_stocked_snacks = set(real_stocked_snacks)

print('real_stocked_snacks: ', real_stocked_snacks, end='\n\n')

print('snackers_email: ', snackers_email, end='\n\n')

print('total_price: ', total_price, end='\n\n')

print('My favourite snack is "Clif Crunch Bar"', end='\n\n')
